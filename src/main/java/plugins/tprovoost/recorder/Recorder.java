package plugins.tprovoost.recorder;

import icy.canvas.IcyCanvas;
import icy.file.FileUtil;
import icy.gui.frame.IcyFrame;
import icy.gui.frame.progress.AnnounceFrame;
import icy.gui.frame.progress.FailedAnnounceFrame;
import icy.gui.frame.progress.ProgressFrame;
import icy.gui.frame.progress.SuccessfullAnnounceFrame;
import icy.gui.viewer.Viewer;
import icy.image.IcyBufferedImage;
import icy.image.IcyBufferedImageUtil;
import icy.image.ImageUtil;
import icy.main.Icy;
import icy.plugin.abstract_.PluginActionable;
import icy.preferences.XMLPreferences;
import icy.resource.ResourceUtil;
import icy.sequence.MetaDataUtil;
import icy.sequence.Sequence;
import icy.system.thread.ThreadUtil;
import icy.type.DataType;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import loci.common.services.ServiceException;
import loci.formats.FormatException;
import loci.formats.meta.MetadataRetrieve;
import loci.formats.out.AVIWriter;

public class Recorder extends PluginActionable
{

    JButton btnAction;
    ImageGrabber grabber = null;
    ImageSaver saver = null;
    JLabel lblImgTaken;
    Icon iconStop;
    Icon iconRun;
    JSpinner tfFramerate;
    LinkedList<BufferedImage> buffer = new LinkedList<BufferedImage>();
    XMLPreferences prefs = getPreferences("prefs");
    JComboBox cboxType;
    GraphicsDevice[] gcs = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();

    /**
     * wbp.parser.entryPoint
     */
    @Override
    public void run()
    {
        JPanel mainPanel = new JPanel();
        mainPanel.setBorder(new EmptyBorder(4, 4, 4, 4));

        // Prepare Icons
        Image img = ResourceUtil.getIconAsImage("alpha/playback_rec.png");
        BufferedImage imgB = ImageUtil.convert(img, null);
        iconRun = loadIcon(imgB, 30, Color.RED);

        img = ResourceUtil.getIconAsImage("alpha/playback_stop.png");
        imgB = ImageUtil.convert(img, null);
        iconStop = loadIcon(imgB, 30, Color.BLACK);

        // GUI
        IcyFrame frame = new IcyFrame("Recorder", false, true, true, true);
        frame.setLayout(new GridLayout());
        frame.add(mainPanel);
        mainPanel.setLayout(new BorderLayout(0, 0));

        JPanel panelButton = new JPanel();
        panelButton.setBorder(new EmptyBorder(2, 4, 2, 4));
        mainPanel.add(panelButton, BorderLayout.CENTER);
        panelButton.setLayout(new BoxLayout(panelButton, BoxLayout.X_AXIS));

        Component horizontalGlue = Box.createHorizontalGlue();
        panelButton.add(horizontalGlue);

        btnAction = new JButton(iconRun);
        btnAction.setSize(20, 20);
        panelButton.add(btnAction);

        panelButton.add(Box.createHorizontalGlue());
        btnAction.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (grabber != null)
                {
                    // stop recording
                    grabber.loop = false;
                    saver.loop = false;
                    grabber = null;
                    saver = null;
                    btnAction.setIcon(iconRun);
                }
                else
                {
                    // start the recording by choosing the file to save
                    JFileChooser fc = new JFileChooser();
                    fc.setFileFilter(new FileNameExtensionFilter("AVI", "avi"));
                    String previous = prefs.get("prev_file", "");
                    if (!previous.equals(""))
                        fc.setCurrentDirectory(new File(previous));
                    if (fc.showDialog(Icy.getMainInterface().getMainFrame(), "Select a file") == JFileChooser.APPROVE_OPTION)
                    {
                        // pre-work on filename such as existence and
                        // extension
                        File selectedFile = fc.getSelectedFile();
                        if (selectedFile.exists())
                            selectedFile.delete();
                        String fileName = selectedFile.getAbsolutePath();
                        String directory = FileUtil.getDirectory(fileName);
                        if (FileUtil.getFileExtension(fileName, false).contentEquals(""))
                            fileName = FileUtil.setExtension(fileName, ".avi");

                        // change the GUI
                        prefs.put("prev_file", directory);
                        lblImgTaken.setText("0");
                        btnAction.setIcon(iconStop);

                        // get the target of the recording
                        int target = cboxType.getSelectedIndex();
                        if (target == 0)
                        {
                            Viewer v = getActiveViewer();
                            if (v == null)
                                return;
                            IcyCanvas canvas = v.getCanvas();

                            // create image grabber
                            grabber = new ImageGrabber(canvas);
                        }
                        else
                        {
                            try
                            {
                                // create a robot with the selected screen
                                Robot robot = new Robot(gcs[target - 1]);

                                // create image grabber
                                grabber = new ImageGrabber(robot, GraphicsEnvironment.getLocalGraphicsEnvironment()
                                        .getMaximumWindowBounds());
                            }
                            catch (AWTException e1)
                            {
                                new FailedAnnounceFrame("Couldn't acquire.");
                                return;
                            }
                        }
                        // create image saver
                        saver = new ImageSaver(fileName);
                        ThreadUtil.bgRun(grabber);
                        ThreadUtil.bgRun(saver);
                    }
                }
            }
        });

        JPanel panelFrameRate = new JPanel();
        panelFrameRate.setBorder(new EmptyBorder(2, 4, 2, 4));
        mainPanel.add(panelFrameRate, BorderLayout.SOUTH);

        JLabel lblFps = new JLabel("FPS: ");
        lblFps.setToolTipText("A high FPS increases memory usage.");
        tfFramerate = new JSpinner(new SpinnerNumberModel(15d, 1.0d, 60d, 1d));
        tfFramerate.setValue(Double.valueOf(prefs.getDouble("FPS", 15d)));
        tfFramerate.setToolTipText("A high FPS increases memory usage.");
        tfFramerate.addChangeListener(new ChangeListener()
        {

            @Override
            public void stateChanged(ChangeEvent e)
            {
                prefs.putDouble("FPS", ((Double) tfFramerate.getValue()).doubleValue());
            }
        });

        panelFrameRate.setLayout(new BoxLayout(panelFrameRate, BoxLayout.X_AXIS));
        panelFrameRate.add(lblFps);
        panelFrameRate.add(tfFramerate);
        Component horizontalStrut = Box.createHorizontalStrut(20);
        panelFrameRate.add(horizontalStrut);

        JLabel lblImagesCaptured = new JLabel("Images captured: ");
        panelFrameRate.add(lblImagesCaptured);
        lblImgTaken = new JLabel("0");
        panelFrameRate.add(lblImgTaken);
        Component horizontalStrut_1 = Box.createHorizontalStrut(20);
        panelFrameRate.add(horizontalStrut_1);

        JPanel panel = new JPanel(new GridLayout(1, 2));
        panel.setBorder(new EmptyBorder(4, 4, 4, 4));
        mainPanel.add(panel, BorderLayout.NORTH);

        JLabel lblselect = new JLabel("Select the target: ");
        panel.add(lblselect);

        String[] values = new String[gcs.length + 1];
        values[0] = "Viewer";
        for (int i = 0; i < gcs.length; ++i)
        {
            GraphicsDevice screen = gcs[i];
            values[i + 1] = "Screen " + screen.getIDstring();
        }
        cboxType = new JComboBox(values);
        panel.add(cboxType);
        cboxType.setToolTipText("Record Target.");

        frame.setSize(250, 135);
        frame.setVisible(true);
        addIcyFrame(frame);
    }

    private static Icon loadIcon(BufferedImage img, int size, Color color)
    {
        Icon icon = new ImageIcon(ImageUtil.paintColorImageFromAlphaImage(
                img.getScaledInstance(size, size, Image.SCALE_SMOOTH), null, color));
        return icon;
    }

    class ImageGrabber implements Runnable
    {

        boolean loop = true;
        IcyCanvas canvas = null;
        Robot robot = null;
        int taken = 0;
        long sleepTime;
        static final long startClockUnit = 1000000000L;

        public ImageGrabber(IcyCanvas canvas)
        {
            this.canvas = canvas;
        }

        public ImageGrabber(Robot robot, Rectangle targetRectangle)
        {
            this.robot = robot;
        }

        @Override
        public void run()
        {
            new AnnounceFrame("Starting in... 3", 3);
            ThreadUtil.sleep(1000);
            new AnnounceFrame("Starting in... 2", 3);
            ThreadUtil.sleep(1000);
            new AnnounceFrame("Starting in... 1", 3);
            ThreadUtil.sleep(1000);

            while (loop)
            {
                ThreadUtil.sleep(sleepTime = (long) (1000d / ((Double) tfFramerate.getValue()).doubleValue()));

                if (canvas == null && robot != null)
                {
                    buffer.add(robot.createScreenCapture(new Rectangle(0, 0, 1280, 1024)));
                }
                else if (canvas != null)
                {
                    BufferedImage captured = canvas.getRenderedImage(canvas.getPositionT(), canvas.getPositionZ(), -1, true);
                    if (captured != null)
                        buffer.add(captured);
                }
                ++taken;
                lblImgTaken.setText("" + taken);
            }
        }
    }

    class ImageSaver implements Runnable
    {

        boolean loop = true;
        String fileName;
        private int imgCount = 0;
        boolean firstWrite = true;
        AVISaver saver;
        ProgressFrame progressFrame;

        public ImageSaver(String fileName)
        {
            this.fileName = fileName;
        }

        @Override
        public void run()
        {
            FileOutputStream fos, fos2;
            DataOutputStream dos, dos2;
            try
            {
                String directory = FileUtil.getDirectory(fileName);
                fos = new FileOutputStream(directory + "saver.tmp");
                dos = new DataOutputStream(fos);

                fos2 = new FileOutputStream(directory + "otherData.tmp");
                dos2 = new DataOutputStream(fos2);
            }
            catch (IOException e1)
            {
                loop = false;
                return;
            }
            progressFrame = new ProgressFrame("Buffering...");
            while (loop || !buffer.isEmpty())
            {
                // No job, wait for an element to be added to the list.
                while (buffer.isEmpty())
                    ThreadUtil.sleep(1);

                progressFrame.setLength(buffer.size());
                progressFrame.setPosition(1);
                try
                {
                    BufferedImage img = buffer.pop();
                    IcyBufferedImage popped = IcyBufferedImage.createFrom(img);
                    int w = popped.getWidth();
                    int h = popped.getHeight();

                    if (firstWrite)
                    {
                        dos2.writeInt(w);
                        dos2.writeInt(h);
                        dos2.writeInt(popped.getSizeC());
                        dos2.writeInt(popped.getDataType_().ordinal());
                        firstWrite = false;
                    }
                    dos.write(popped.getRawData(false));
                    ++imgCount;
                }
                catch (NoSuchElementException e)
                {
                    e.printStackTrace();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            try
            {
                dos2.writeInt(imgCount);

                fos.close();
                fos2.close();
                progressFrame.close();

                // Start Recontructor
                ThreadUtil.bgRun(new Recontructor(fileName));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    class Recontructor implements Runnable
    {

        String fileName;
        ProgressFrame progressFrame = new ProgressFrame("Creating AVI...");

        public Recontructor(String fileName)
        {
            this.fileName = fileName;
        }

        @Override
        public void run()
        {
            FileInputStream fis, fis2;
            DataInputStream dis, dis2;
            AVISaver saver;
            try
            {
                String directory = FileUtil.getDirectory(fileName);
                fis = new FileInputStream(directory + "saver.tmp");
                dis = new DataInputStream(fis);

                fis2 = new FileInputStream(directory + "otherData.tmp");
                dis2 = new DataInputStream(fis2);

                int w = dis2.readInt();
                int h = dis2.readInt();
                int sizeC = dis2.readInt();
                DataType type = DataType.values()[dis2.readInt()];
                int imgCount = dis2.readInt();

                if ((w < 1) || (h < 1) || (sizeC < 3) || (imgCount < 1))
                {
                    System.out.println("Error: no image to save...");
                    dis.close();
                    dis2.close();
                    return;
                }

                if (sizeC == 3)
                    saver = new AVISaver(new AVIWriter(), fileName, w, h, 3, imgCount, type);
                else if (sizeC == 4)
                    saver = new AVISaver(new AVIWriter(), fileName, w, h, 3, imgCount, type);
                else
                {
                    System.out.println("Error: not RGB or ARGB");
                    dis.close();
                    dis2.close();
                    return;
                }

                if (!saver.initialized)
                {
                    System.out.println("Error: cannot initialize saver");
                    dis.close();
                    dis2.close();
                    return;
                }

                progressFrame.setLength(imgCount);

                int i = 0;
                while (i < imgCount)
                {
                    progressFrame.setPosition(i);
                    try
                    {
                        byte[] rawData = new byte[w * h * type.getSize() * sizeC];
                        IcyBufferedImage img = new IcyBufferedImage(w, h, sizeC, type);

                        if (dis.read(rawData, 0, w * h * type.getSize() * sizeC) <= 0)
                            break;

                        img.setRawData(rawData, false);

                        if (sizeC == 3)
                            saver.addImageToWriter(img);
                        else
                            saver.addImageToWriter(IcyBufferedImageUtil.extractChannels(img, 0, 1, 2));

                        ++i;
                    }
                    catch (EOFException e)
                    {
                        break;
                    }
                }
                fis.close();
                fis2.close();
                saver.writer.close();
                File toDeleteFile = new File(directory + "saver.tmp");
                toDeleteFile.delete();
                toDeleteFile = new File(directory + "otherData.tmp");
                toDeleteFile.delete();
                progressFrame.close();
                
                new SuccessfullAnnounceFrame("AVI Successfully Created.", 5);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private class AVISaver
    {

        AVIWriter writer;
        boolean littleEndian;
        final boolean separateChannel = false;
        private int index = 0;
        boolean initialized = false;

        AVISaver(AVIWriter writer, String fileName, int w, int h, int sizeC, int imgCount, DataType type)
        {
            this.writer = writer;
            
            try
            {
                // set settings
                writer.setFramesPerSecond(((Double) tfFramerate.getValue()).intValue());
                // set metadata
                writer.setMetadataRetrieve((MetadataRetrieve) MetaDataUtil.generateMetaData(w, h, sizeC, 1, imgCount, type,
                        separateChannel));
                // interleaved flag
                writer.setInterleaved(false);
                // set id
                writer.setId(fileName);
                // init
                writer.setSeries(0);
                // usually give better save performance
                writer.setWriteSequentially(true);
                littleEndian = !writer.getMetadataRetrieve().getPixelsBinDataBigEndian(0, 0).booleanValue();

                initialized = true;
            }
            catch (FormatException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (ServiceException e)
            {
                e.printStackTrace();
            }
        }

        public void addImageToWriter(IcyBufferedImage img)
        {
            try
            {
                // System.out.println(img);
                writer.saveBytes(index, img.getRawData(littleEndian));
                ++index;
            }
            catch (FormatException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
